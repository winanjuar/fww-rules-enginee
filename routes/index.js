const express = require("express");
const router = express.Router();
const fs = require("fs");
const path = require("path");
const { Engine } = require("json-rules-engine");
const logger = require("../logger");

/* GET home page. */
router.post("/api/v1/check-discount-destination", function (req, res, next) {
  const ruleFile = "discount-destination.json";
  const fullPathRule = path.join(process.env.ASSET_PATH, ruleFile);
  const completeRules = JSON.parse(fs.readFileSync(fullPathRule, "utf8"));
  const engine = new Engine();
  engine.addRule(completeRules.decisions[0]);

  const destination = req.body.destination || "";
  const facts = { destination: destination.toUpperCase() };

  engine.run(facts).then(({ events }) => {
    events.forEach((event) => logger.info(event.params.message));
  });

  engine.on("success", (event) => {
    logger.info("Rules discount destination fulfilled");
    res.json({
      statusRule: "Fulfilled",
      discountDestination: event.params.discountDestination,
    });
  });

  engine.on("failure", () => {
    logger.info("Rules discount destination not fulfilled");
    res.json({ statusRule: "Not Fulfilled", discountDestination: 0 });
  });
});

router.post("/api/v1/check-discount-fast-payment", function (req, res, next) {
  const ruleFile = "discount-fast-payment.json";
  const fullPathRule = path.join(process.env.ASSET_PATH, ruleFile);
  const completeRules = JSON.parse(fs.readFileSync(fullPathRule, "utf8"));
  const engine = new Engine();
  engine.addRule(completeRules.decisions[0]);

  const minutesDurationFromBooking = req.body.minutesDurationFromBooking || -1;
  const facts = { minutesDurationFromBooking };

  engine.run(facts).then(({ events }) => {
    events.forEach((event) => logger.info(event.params.message));
  });

  engine.on("success", (event) => {
    logger.info("Rules discount fast payment fulfilled");
    res.json({
      statusRule: "Fulfilled",
      discountFastPayment: event.params.discountFastPayment,
    });
  });

  engine.on("failure", () => {
    logger.info("Rules discount fast payment not fulfilled");
    res.json({ statusRule: "Not Fulfilled", discountFastPayment: 0 });
  });
});

module.exports = router;
